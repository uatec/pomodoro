/// <reference path="../typings/index.d.ts" />

import FlatButton from "material-ui/FlatButton";
import LinearProgress from 'material-ui/LinearProgress';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import * as React from "react";
import * as ReactDOM from "react-dom";
import * as injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();
import { Hello } from "./components/Hello";

var Thing = React.createClass({
    getInitialState() {
        return {
            status: 'stopped',
            lastStarted: new Date(),
            workPeriod: 25,
            restPeriod: 5
        };
    },

    update() {
        var now = new Date();
        var time = (now.getTime() - this.state.lastStarted.getTime());
        console.log(time, this.state.workPeriod * 1000);
        var progress = 0;
        if ( time < this.state.workPeriod * 1000 ) progress =  100 * time / (this.state.workPeriod * 1000);
        else if ( time < (this.state.workPeriod + this.state.restPeriod) * 1000 ) progress =  1 - (100 * (time - this.state.workPeriod * 1000) / (this.state.restPeriod * 1000));
        else {
            this.setState({
                lastStarted : new Date(),
                progress: 0
            });
            return;
        }

        this.setState({
            progress: progress
        });
    },

    componentDidMount()
    {
        setInterval(this.update.bind(this), 250);
    },

    render() {
        return <div style={{marginTop: '24px'}}>
            <FlatButton label="Start" primary={true} />
            <LinearProgress mode="determinate" value={this.state.progress} />
        </div>;
    }
});

ReactDOM.render(
    <MuiThemeProvider>
        <Thing />
    </MuiThemeProvider>,
    document.getElementById("example")
);